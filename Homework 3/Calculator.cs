﻿namespace Homework_3
{
    internal class Calculator
    {
        public static void Calculate()
        {
            while (true)
            {
                ArithmeticOperation operation;

                Console.Write("Input first number: ");

                if (!double.TryParse(Console.ReadLine(), out var firstOperand))
                {
                    Console.WriteLine("Invalid value entered. You only need to enter numbers");
                    continue;
                }

                Console.Write("Input second number: ");
                if (!double.TryParse(Console.ReadLine(), out var secondOperand))
                {
                    Console.WriteLine("Invalid value entered. You only need to enter numbers");
                    continue;
                }

                Console.Write("Input operation sign (+, -, *, /): ");
                char operationSymbol = Console.ReadKey().KeyChar;
                Console.WriteLine();

                if (Enum.IsDefined(typeof(ArithmeticOperation), (int)operationSymbol))
                {
                    operation = (ArithmeticOperation)operationSymbol;

                    switch (operation)
                    {
                        case ArithmeticOperation.Addition:
                            Console.WriteLine($"Result: {firstOperand} + {secondOperand} = {firstOperand + secondOperand}");
                            break;
                        case ArithmeticOperation.Subtraction:
                            Console.WriteLine($"Result: {firstOperand} - {secondOperand} = {firstOperand - secondOperand}");
                            break;
                        case ArithmeticOperation.Multiplication:
                            Console.WriteLine($"Result: {firstOperand} * {secondOperand} = {firstOperand * secondOperand}");
                            break;
                        case ArithmeticOperation.Division:
                            if (secondOperand != 0)
                                Console.WriteLine($"Result: {firstOperand} / {secondOperand} = {firstOperand / secondOperand}");
                            else
                                Console.WriteLine("Error! You can't divide by zero.");
                            break;
                        default:
                            Console.WriteLine("Invalid operation.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid operation");
                }

                Console.WriteLine("Input 'q' to exit or press 'Enter' to continue: ");
                if (Console.ReadKey().KeyChar == 'q')
                    break;
            }
        }
    }
}
