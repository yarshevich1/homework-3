﻿namespace Homework_3
{
    internal class ParityCheck
    {
        public static void CheckNumberParity()
        {
            while (true)
            {
                Console.Write("Input number: ");

                if (!int.TryParse(Console.ReadLine(), out var result))
                {
                    Console.WriteLine("Invalid value entered. You only need to enter number");
                    continue;
                }

                if (result % 2 == 0)
                    Console.WriteLine("Number is even");
                else
                    Console.WriteLine("Number isn't even");


                Console.WriteLine("Input 'q' to exit or press 'Enter' to continue: ");
                if (Console.ReadKey().KeyChar == 'q')
                    break;
            }
        }
    }
}
