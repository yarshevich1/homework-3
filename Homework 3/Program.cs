﻿namespace Homework_3
{
    internal class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Choose tool: \n1. Calculator\n2. Among numbers\n3. Translator\n4. Parity check");

                if (!int.TryParse(Console.ReadLine(), out var tool))
                {
                    Console.WriteLine("Invalid value entered. You only need to enter numbers");
                    continue;
                }


                switch (tool)
                {
                    case 1: Calculator.Calculate();
                        break;
                    case 2:
                        AmongNumbers.FoundNumber();
                        break;
                    case 3:
                        Translator.Translate();
                        break;
                    case 4:
                        ParityCheck.CheckNumberParity();
                        break;
                    default:
                        Console.WriteLine("You must enter number from 1 to 4");
                        break;
                }
            }
        }
    }
}