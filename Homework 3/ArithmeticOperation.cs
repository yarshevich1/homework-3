﻿namespace Homework_3
{
    internal enum ArithmeticOperation
    {
        Addition = '+',
        Subtraction = '-',
        Multiplication = '*',
        Division = '/'
    }
}
