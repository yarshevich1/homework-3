﻿namespace Homework_3
{
    internal class AmongNumbers
    {
        public static void FoundNumber()
        {
            while (true)
            {
                Console.Write("Input number: ");
                if (!int.TryParse(Console.ReadLine(), out var numberResult))
                    Console.WriteLine("Invalid value entered. You only need to enter number");

                if (numberResult >= 0 && numberResult <= 14)
                    Console.WriteLine("Number between [0 - 14]");
                else if (numberResult >= 15 && numberResult <= 35)
                    Console.WriteLine("Number between [15 - 35]");
                else if (numberResult >= 36 && numberResult <= 49)
                    Console.WriteLine("Number between [36 - 49]");
                else if (numberResult >= 50 && numberResult <= 100)
                    Console.WriteLine("Number between [50 - 100]");
                else
                    Console.WriteLine("The number is not included in the specified intervals.");

                Console.WriteLine("Input 'q' to exit or press 'Enter' to continue: ");
                if (Console.ReadKey().KeyChar == 'q')
                    break;
            }
        }
    }
}
