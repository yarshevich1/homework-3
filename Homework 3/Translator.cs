﻿namespace Homework_3
{
    internal class Translator
    {
        static Dictionary<string, string> WeatherDictionary { get; } = new Dictionary<string, string>
        {
            { "солнечно", "sunny" },
            { "облачно", "cloudy" },
            { "дождь", "rain" },
            { "снег", "snow" },
            { "ветрено", "windy" },
            { "тепло", "warm" },
            { "холодно", "cold" },
            { "гроза", "storm" },
            { "туман", "fog" },
            { "влажность", "humidity" }
        };

        public static void Translate()
        {
            while (true)
            {
                Console.WriteLine("Enter word in Russian or English about the weather");
                string input = Console.ReadLine();

                if (WeatherDictionary.ContainsKey(input.ToLower()))
                {
                    string translation = WeatherDictionary[input.ToLower()];
                    Console.WriteLine($"Translated word '{input}' on English: {translation}");
                }
                else if (WeatherDictionary.ContainsValue(input.ToLower()))
                {
                    string originalWord = WeatherDictionary.FirstOrDefault(x => x.Value == input.ToLower()).Key;
                    Console.WriteLine($"Translated word '{input}'  on Russian: {originalWord}");
                }
                else
                {
                    Console.WriteLine("The word was not found in the dictionary.");
                }

                Console.WriteLine("Input 'q' to exit or press 'Enter' to continue: ");
                if (Console.ReadKey().KeyChar == 'q')
                    break;
            }
        }
    }
}
